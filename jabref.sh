#!/usr/bin/env zsh

lib="$HOME/Documents/Notes/library.bib"
jab="$HOME/Sync/JabRef"

f1=`mktemp`
f2=`mktemp`
cat $lib | sort | egrep " *file *\= *{" | awk '{print $3}' | awk 'FS=":" {print $2}' | sort | awk '!/^[[:blank:]]*$/' > $f1
ls -la $jab/* | awk '{print $9}' | awk 'FS="/" {print $NF}' | sort | awk '!/^[[:blank:]]*$/' > $f2
l1=`cat $f1 | wc -l`
l2=`cat $f2 | wc -l`

echo "Comparing JabRef .bib library with JabRef attachments folder"
echo
echo "Files in the library file: $l1"
echo "Files in the attachments folder: $l2"
echo
echo "Non-matching files:"
grep -Fxvf $f1 $f2
rm -f $f1
rm -f $f2