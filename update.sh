#!/usr/bin/env zsh

# store initial directory
pwd=$PWD

# update Debian packages
echo "Updating Debian packages"
sudo apt update
sudo apt dist-upgrade
sudo apt autoremove
sudo apt autoclean

# update Julia packages
echo "Updating Julia packages"
julia -q --color=yes -O2 -g0 --cpu-target=native -e "using Pkg; Pkg.update()"

# update Git repositories
echo "Updating Git repositories"
cd ~/Documents/Code/
find . -type d -exec zsh -c 'cd $0; if [ -d .git ]; then echo "Pulling $0"; git pull; fi' {} \;
cd ~/Documents/Code_misc/
find . -type d -exec zsh -c 'cd $0; if [ -d .git ]; then echo "Pulling $0"; git pull; fi' {} \;

echo "Updating Notes"
cd ~/Documents/Notes
git pull

echo "Updating BBJ"
cd ~/Documents/BBJ
git pull

echo "Updating FreeMind"
cd ~/Documents/FreeMind
git pull

echo "Updating Config"
cd ~/Documents/Config
git pull

echo "Updating CSL styles"
cd ~/Documents/Research/CSL\ styles
git pull

# return to the initial directory
cd $pwd
