#!/usr/bin/env zsh

# Download YouTube videos in parallel
# Arguments: YouTube URL (channel or playlist)
# Requirements: awk, xargs, yt-dlp

echo "Parallel Youtube DownLoader"
echo "==========================="
echo

if [[ `which awk` == "awk not found" ]]; then
    echo "Could not find awk, exiting."
    exit -1
fi
if [[ `which yt-dlp` == "yt-dlp not found" ]]; then
    echo "Could not find yt-dlp, exiting."
    exit -1
fi
if [[ `which xargs` == "xargs not found" ]]; then
    echo "Could not find xargs, exiting."
    exit -1
fi

# resume download or start new
if [[ "$1" == "" ]]; then
    id=`ls | grep .pydl`
    if [ ! -f $id ]; then
        echo "No URL provided and cannot resume download, exiting."
        echo
        exit -1
    else
        echo "Resuming download"
        echo
    fi
else
    url=`echo $1 | sed 's/\/videos//g'` 
    echo $url > URL.txt
    # generate ID folder
    id=`echo $url | awk -v FS="/" '{print $NF}'`
    if [[ "$1" == *"channel"* ]]; then
        id=`echo $id | awk -v FS="=" '{print $NF}'`
        echo "URL contains channel; ID: $id"
    elif [[ "$id" == *"playlist"* ]]; then
        id=`echo $id | awk -v FS="=" '{print $NF}'`
        echo "URL contains playlist; ID: $id"
    elif [[ "$id" == *"list"* ]]; then
        id=`echo $id | awk -v FS="=" '{print $NF}'`
        echo "URL contains playlist; ID: $id"
    elif [[ "$id" == *"@"* ]]; then
        id=`echo $id | awk -v FS="@" '{print $NF}'`
        echo "URL contains creator; ID: $id"
    elif [[ "$id" == *"user"* ]]; then
        id=`echo $id | awk -v FS="@" '{print $NF}'`
        echo "URL contains creator; ID: $id"
    fi

    # generate movie IDs
    if [ ! -d $id ]; then
        mkdir -p $id
        mv URL.txt $id
        cd $id
        id=$id".pydl"
        echo
        echo "Generating list of IDs"
        yt-dlp --ignore-errors --get-id $1 | tee $id
        echo
    else
        cd $id
        id=$id".pydl"
        echo
        echo "Resuming download"
        echo
    fi
fi

# default number of parallel downloads is 10
if [[ "$2" == "" ]]; then
    np="10"
else
    np="$2"
fi

# recreate ARCHIVE.txt file
find -name "*.mkv" -exec basename {} .mkv \; | sed 's/\[//' | sed 's/\]//' | grep -o '.\{11\}$' | sed 's/^/youtube /' > ARCHIVE.txt
find -name "*.mp4" -exec basename {} .mp4 \; | sed 's/\[//' | sed 's/\]//' | grep -o '.\{11\}$' | sed 's/^/youtube /' >> ARCHIVE.txt
find -name "*.webm" -exec basename {} .webm \; | sed 's/\[//' | sed 's/\]//' | grep -o '.\{11\}$' | sed 's/^/youtube /' >> ARCHIVE.txt

# parallel download
l1=`cat $id | wc -l`
echo "Downloading $l1 videos ("$np" parallel downloads)"
cat $id | xargs -I '{}' -t -P "$np" -t yt-dlp --ignore-errors -q --no-warnings --download-archive ARCHIVE.txt 'https://youtube.com/watch?v={}'
echo

# cleanup
l2=`ls *.part | wc -l` 2>/dev/null
if [ $l2 -ne 0 ]; then
    if [[ $l2 == 1 ]]; then
        echo "Re-downloading $l2 incomplete file"
    else
        echo "Re-downloading $l2 incomplete files"
    fi
    cat $id | xargs -I '{}' -t -P 5 -t yt-dlp --ignore-errors -q --no-warnings --download-archive ARCHIVE.txt 'https://youtube.com/watch?v={}'
    echo "All files downloaded"
    rm -f $id
else
    echo "All files downloaded"
    rm -f $id
fi

cd ..

exit 0
