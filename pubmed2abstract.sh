#!/usr/bin/env zsh

tmp1=`mktemp`
tmp2=`mktemp`
abstract=`mktemp`

# check dependencies
if [[ `which curl` == "curl not found" ]]; then
    echo "Could not find curl, exiting."
    exit -1
fi
if [[ `which xclip` == "xclip not found" ]]; then
    echo "Could not find xclip, exiting."
    exit -1
fi

# check argument
if [[ "$1" == "" ]]; then
    echo "No PMID/PMC/DOI provided, exiting."
    exit -1
fi

# convert to PMID
pmid=`curl "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&ids=$1&format=json" 2>/dev/null | grep "pmid" | awk 'FS=":" {print $2}' | tr -d '"' | tr -d ','`
if [[ $pmid == "" ]]; then
    echo "Could not identify PMID, exiting."
    exit -1
fi
echo "Downloading PMID: $pmid"
curl "https://pubmed.ncbi.nlm.nih.gov/$pmid/?format=pubmed" 2>/dev/null > $tmp1

# check if abstract is available
if [[ `cat $tmp1 | grep "AB  - " | wc -l` == 0 ]]; then
    echo "No abstract found, exiting."
    exit -1
fi

# extract PubMed record
l1="$((`cat $tmp1 | grep -n "article-details" | cut -d : -f 1`+1))"
l2="$((`cat $tmp1 | grep -n "</pre>" | cut -d : -f 1`-$l1))"

# extract abstract
cat $tmp1 | tail -n+$l1 | head --lines=$l2 > $tmp2
# beginning of abstract
l1=`cat $tmp2 | grep -n "AB  - " | cut -d : -f 1`
# end of abstract
l=`cat $tmp2 | tail -n+$(($l1+1)) | grep -v -m1 "      " | cut -d : -f 1`
l2="$((`cat $tmp2 | grep -n $l | cut -d : -f 1`-$l1))"
# remove field name and multiple spaces
cat $tmp2 | tail -n+$l1 | head --lines=$l2 | sed 's/AB  - //g' | sed 's/  */ /g' > $abstract

# remove EOLs and copy to clipboard
cat $abstract | tr -d '\r\n' | xclip -selection clipboard

# cleanup
rm -f $tmp1
rm -f $tmp2
rm -f $abstract

echo "Abstract copied to the clipboard."
exit 0